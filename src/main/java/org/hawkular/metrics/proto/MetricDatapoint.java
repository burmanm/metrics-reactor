/*
 * Copyright 2017 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.hawkular.metrics.proto;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author michael
 */
public class MetricDatapoint {
    public byte metricType;
    public String id;
    public String tenantId;
    public Datapoint[] data;
    @JsonIgnore
    public Map<String, String> tags;

    public MetricDatapoint() {}

    public MetricDatapoint(@JsonProperty("id") String id, @JsonProperty("data") Datapoint[] data) {
        this.id = id;
        this.data = data;
    }

    public MetricDatapoint(String tenantId, byte metricType, String id, Datapoint[] data) {
        this.metricType = metricType;
        this.tenantId = tenantId;
        this.data = data;
        this.id = id;
    }

    public byte getMetricType() {
        return metricType;
    }

    public String getTenantId() {
        return tenantId;
    }

    public Datapoint[] getData() {
        return data;
    }

    public String getId() {
        return id;
    }

    public void setMetricType(byte metricType) {
        this.metricType = metricType;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }
}
