/*
 * Copyright 2017 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.hawkular.metrics.proto;

import static com.datastax.driver.core.BatchStatement.Type.UNLOGGED;

import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

import org.hawkular.metrics.proto.json.DatapointIterator;
import org.reactivestreams.Publisher;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.ResultSetFuture;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.querybuilder.Batch;
import com.datastax.driver.core.utils.UUIDs;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.jsoniter.DecodingMode;
import com.jsoniter.JsonIterator;
import com.jsoniter.annotation.JsoniterAnnotationSupport;
import com.jsoniter.output.EncodingMode;
import com.jsoniter.output.JsonStream;

import io.netty.handler.codec.http.HttpResponseStatus;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.WorkQueueProcessor;
import reactor.core.scheduler.Schedulers;
import reactor.ipc.netty.NettyContext;
import reactor.ipc.netty.http.server.HttpServer;
import reactor.ipc.netty.http.server.HttpServerRequest;
import reactor.ipc.netty.http.server.HttpServerResponse;

/**
 * @author michael
 */
public class ProtoHttpServer {

    private NettyContext context;
    private WorkQueueProcessor<MetricDatapoint> queue;
    private Disposable subscriber;
    private BoundStatement boundStatement;

//    private ObjectReader reader = new ObjectMapper().readerFor(Datapoint[].class);

    private CassandraManager manager;
    private Session session;
    PreparedStatement insertGaugeData;

    BiFunction<? super HttpServerRequest, ? super HttpServerResponse, ? extends Publisher<Void>> tenantFilter() {
        return (req, resp) -> {
            String tenantId = req.requestHeaders().get("Hawkular-Tenant");
            if(tenantId == null || tenantId.length() < 1) {

            }
            return resp;
        };
    }

    BiFunction<? super HttpServerRequest, ? super HttpServerResponse, ? extends Publisher<Void>> insertMultiHandler() {
        return (req, resp) -> {
            String tenantId = req.requestHeaders().get("Hawkular-Tenant");
            String type = req.param("type");
            String id = req.param("id");

            return req
                    .receive()
                    .aggregate()
                    .asByteArray()
//                    .subscribeOn(Schedulers.elastic())
                    // Parse JSON
                    .flatMap(b -> DatapointIterator.jsonToMetricDatapointFlux(JsonIterator.parse(b)))
//                    .flatMap(b -> {
//                        try {
//                            return Flux.fromArray((MetricDatapoint[]) reader.forType(MetricDatapoint[].class).readValue
//                                    (b));
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                            throw Exceptions.propagate(e);
//                        }
//                    })
//                    .subscribeOn(Schedulers.newParallel("process-jackson", 128))
                    // Enrich to MetricDatapoint
                    .map(m -> {
                        m.setMetricType((byte) 0);
                        m.setTenantId(tenantId);
                        return m;
                    })
                    // Map to BoundStatement
                    .map(d ->
                            insertGaugeData.bind()
                                    .setDouble(0, d.getData()[0].value)
                                    .setString(1, d.getTenantId())
                                    .setByte(2, d.getMetricType())
                                    .setString(3, d.getId())
                                    .setLong(4, 0)
                                    .setUUID(5, UUIDs.startOf(d.getData()[0].timestamp)))
                    // Micro-batching (see Hawkular-Metrics for real-world implementation)
                    .window(50)
                    .flatMap(window -> window
                            .collect(() -> new BatchStatement(UNLOGGED),
                                    BatchStatement::add))
                    // Push to storage
                    .flatMap(batchStatement -> {
                        CompletableFuture<ResultSet> future = new CompletableFuture<>();
                        ResultSetFuture insertFuture = session.executeAsync(batchStatement);

                        insertFuture.addListener(() -> future.complete(insertFuture.getUninterruptibly()), Runnable::run);

                        return Mono.fromFuture(future);
                    })
//                    .doOnNext(datapoint -> queue.onNext(datapoint))
                    // Return OK to the caller if everything went alright..
                    .thenEmpty(resp.status(HttpResponseStatus.OK))
                    // And error if things didn't go right
                    .otherwise(t -> resp.status(HttpResponseStatus.BAD_REQUEST).send());
        };
    }

    BiFunction<? super HttpServerRequest, ? super HttpServerResponse, ? extends Publisher<Void>> insertHandler() {
        return (req, resp) -> {

            String tenantId = req.requestHeaders().get("Hawkular-Tenant");
            String type = req.param("type");
            String id = req.param("id");

            return req
                    .receive()
                    .aggregate()
                    .asByteArray()
                    // Parse JSON
                    .flatMap(b -> DatapointIterator.jsonToDatapointFlux(JsonIterator.parse(b)))
//                    .flatMap(b -> {
//                        try {
//                            return Flux.fromArray(reader.forType(Datapoint[].class).readValue(b));
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                            throw Exceptions.propagate(e);
//                        }
//                    })
                    // Enrich to MetricDatapoint
                    .map(d -> new MetricDatapoint(tenantId, (byte) 0, id, new Datapoint[]{d}))
//                    .map(d -> insertGaugeData.bind()
//                            .setDouble(0, d.getData()[0].value)
//                            .setString(1, d.getTenantId())
//                            .setByte(2, d.getMetricType())
//                            .setString(3, d.getId())
//                            .setLong(4, 0)
//                            .setUUID(5, UUIDs.startOf(d.getData()[0].timestamp)))
//                    .subscribeOn(Schedulers.newParallel("process-jackson", 128))
                    // Push to storage
//                    .flatMap(boundStatement -> {
//                        BoundStatement boundStatement = insertGaugeData.bind()
//                                .setDouble(0, d.getData()[0].value)
//                                .setString(1, d.getTenantId())
//                                .setByte(2, d.getMetricType())
//                                .setString(3, d.getId())
//                                .setLong(4, 0)
//                                .setUUID(5, UUIDs.startOf(d.getData()[0].timestamp));
//
//                        CompletableFuture<ResultSet> future = new CompletableFuture<>();
//                        ResultSetFuture insertFuture = session.executeAsync(boundStatement);
//
//                        insertFuture.addListener(() -> future.complete(insertFuture.getUninterruptibly()), Runnable::run);
//
//                        return Mono.fromFuture(future);
//                    })
//                    .subscribeOn(Schedulers.elastic())
//                    .subscribeOn(Schedulers.elastic())
//                    .doOnNext(datapoint -> queue.onNext(datapoint))
                    // Return OK to the caller if everything went alright..
                    .thenEmpty(resp.status(HttpResponseStatus.OK))
                    // And error if things didn't go right
                    .otherwise(t -> resp.status(HttpResponseStatus.BAD_REQUEST).send());
        };
    }

    public void setupProcessing() {
        JsonIterator.setMode(DecodingMode.DYNAMIC_MODE_AND_MATCH_FIELD_WITH_HASH);
        JsonStream.setMode(EncodingMode.DYNAMIC_MODE);
        JsoniterAnnotationSupport.enable();

        queue = WorkQueueProcessor.create("datapoint-sink", 1024);
        manager = new CassandraManager();
        session = manager.createSession();

        insertGaugeData = session.prepare(
                "UPDATE data " +
                        "SET n_value = ? " +
                        "WHERE tenant_id = ? AND type = ? AND metric = ? AND dpart = ? AND time = ? ");

        boundStatement = insertGaugeData.bind()
                .setDouble(0, 1.2)
                .setString(1, "test.aa")
                .setByte(2, (byte) 0)
                .setString(3, "test.metric")
                .setLong(4, 0)
                .setUUID(5, UUIDs.startOf(System.currentTimeMillis()));

        //
//        queue
//                .subscribeOn(Schedulers.parallel())
//                .subscribe();

//        // Imaginary subscriber that in the background does datastorage
//        subscriber = getCassandraSubscriber();
    }

    private Disposable getCassandraSubscriber(Flux<MetricDatapoint> flux) {
        return flux
                .flatMap(d -> {
                    BoundStatement boundStatement = insertGaugeData.bind()
                            .setDouble(0, d.getData()[0].value)
                            .setString(1, d.getTenantId())
                            .setByte(2, d.getMetricType())
                            .setString(3, d.getId())
                            .setLong(4, 0)
                            .setUUID(5, UUIDs.startOf(d.getData()[0].timestamp));

                    CompletableFuture<ResultSet> future = new CompletableFuture<>();
                    ResultSetFuture insertFuture = session.executeAsync(boundStatement);

                    insertFuture.addListener(() -> future.complete(insertFuture.getUninterruptibly()), Runnable::run);

                    return Mono.fromFuture(future);
                })
                .subscribeOn(Schedulers.elastic())
                .subscribe();
    }


    public void startHttpServer() {
        context = HttpServer.create(8556)
                // Preprocess tenant requirement?
                // Add routers
                .newRouter(r -> r
                        .post("/hawkular/metrics/{type}/{ids}/raw", insertHandler())
                        .post("/hawkular/metrics/{type}/raw", insertMultiHandler())
                        .route(httpServerRequest -> true, (req, resp) -> {
                            System.out.printf("Failed: %s %s %s\n", req.path(), req.method().name(), req.uri());
                            return resp.send();
                        })
                )
                .block();

        context.onClose().block();
    }

    public void close() {
        context.dispose();
        subscriber.dispose();
        manager.shutdown();
    }

    public static void main(String[] args) {
        ProtoHttpServer proto = new ProtoHttpServer();
        proto.setupProcessing();
        proto.startHttpServer();
        proto.close();
    }
}
