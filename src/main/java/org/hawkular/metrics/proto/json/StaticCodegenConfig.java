/*
 * Copyright 2017 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.hawkular.metrics.proto.json;

import org.hawkular.metrics.proto.Datapoint;
import org.hawkular.metrics.proto.MetricDatapoint;

import com.jsoniter.spi.CodegenConfig;
import com.jsoniter.spi.TypeLiteral;

/**
 * @author michael
 */
public class StaticCodegenConfig implements CodegenConfig {
    @Override public void setup() {
//        JsoniterSpi.registerPropertyEncoder(Datapoint.class, "tags", new Decoder());
    }

    @Override public TypeLiteral[] whatToCodegen() {
        return new TypeLiteral[]{
                TypeLiteral.create(Datapoint.class),
                TypeLiteral.create(MetricDatapoint.class)
        };
    }
}
