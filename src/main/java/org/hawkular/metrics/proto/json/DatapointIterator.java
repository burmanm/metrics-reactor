/*
 * Copyright 2017 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.hawkular.metrics.proto.json;

import java.io.IOException;

import org.hawkular.metrics.proto.Datapoint;
import org.hawkular.metrics.proto.MetricDatapoint;

import com.jsoniter.JsonIterator;

import reactor.core.publisher.Flux;

/**
 * @author michael
 */
public class DatapointIterator {
    public static Flux<Datapoint> jsonToDatapointFlux(JsonIterator iter) {
        return Flux.create(sink -> {
            try {
                while(iter.readArray()) { // Start of list for datapoints
                    Datapoint read = iter.read(Datapoint.class);
                    sink.next(read);
                }
                iter.close();
            } catch (IOException e) {
                sink.error(e);
            }

            sink.complete();
        });
    }

    public static Flux<MetricDatapoint> jsonToMetricDatapointFlux(JsonIterator iter) {
        return Flux.create(sink -> {
            try {
                while(iter.readArray()) { // Start of list for datapoints
                    MetricDatapoint read = iter.read(MetricDatapoint.class);
                    sink.next(read);
                }
                iter.close();
            } catch (Exception e) {
                e.printStackTrace(); // For debugging purposes only
                sink.error(e);
            }

            sink.complete();
        });
    }
}
